﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using System.Runtime.CompilerServices;


namespace PK.Test
{
    public static class Helpers
    {
        public static void Should_Not_Have_Public_Fields(Type t)
        {
            // Arrange
            var fields = t.GetFields();

            // Assert
            Assert.That(fields, Is.Null.Or.Empty);
        }

        public static void Should_Have_Methods(Type t)
        {
            // Arrange
            var methods = t.GetMethods();

            // Assert
            Assert.That(methods, Is.Not.Empty);
        }

        public static void Should_Have_At_Least_Methods(Type t, int count)
        {
            // Arrange
            var methods = t.GetMethods();

            // Assert
            Assert.That(methods, Has.Length.GreaterThanOrEqualTo(count));
        }

        public static void Should_Have_Method(Type t, string methodName)
        {
            // Arrange
            var method = t.GetMethod(methodName);

            // Assert
            Assert.That(method, Is.Not.Null);
        }

        public static void Should_Inherit_From(Type y, Type x)
        {
            // Assert
            Assert.That(y.IsSubclassOf(x));
        }

        public static void Should_Implement_Interface(Type y, Type x)
        {
            // Arrange
            var interfaces = y.GetInterfaces();

            // Assert
            Assert.That(interfaces, Has.Member(x));
        }

        public static void Should_Implement_Method(Type x, string methodName, object[] parameters)
        {
            // Arrange
            var instance = Activator.CreateInstance(x);
            var method = x.GetMethod(methodName);

            // Act
            method.Invoke(instance, parameters);
        }

        public static void Should_Have_Number_Of_Methods_Between(Type type, int low, int up)
        {
            // Arrange
            var methods = type.GetMethods();

            // Assert
            Assert.That(methods, Has.Length.AtLeast(low).And.Length.AtMost(up));
        }

        public static void Should_Have_An_Extension_Method_For(Type x, Type y)
        {
            // Arrange
            var methods  = from method in x.GetMethods(BindingFlags.Static
                            | BindingFlags.Public | BindingFlags.NonPublic)
                        where method.IsDefined(typeof(ExtensionAttribute), false)
                        where method.GetParameters()[0].ParameterType == y
                        select method;

            // Assert
            Assert.That(methods, Is.Not.Empty);
        }

        public static IEnumerable<MethodInfo> GetExtensionMethods(Assembly assembly, Type extendedType)
        {
            var query = from type in assembly.GetTypes()
                        where type.IsSealed && !type.IsGenericType && !type.IsNested
                        from method in type.GetMethods(BindingFlags.Static
                            | BindingFlags.Public | BindingFlags.NonPublic)
                        where method.IsDefined(typeof(ExtensionAttribute), false)
                        where method.GetParameters()[0].ParameterType == extendedType
                        select method;
            return query;
        }

        public static void Should_Have_Only_Interfaces_Or_Enums_Or_Dependent_Types(Assembly assembly)
        {
            // Arrange
            var types = assembly.GetTypes();
            var usedTypes = new HashSet<Type>();
            foreach (var type in types)
            {
                if (type.IsInterface)
                {
                    foreach (var method in type.GetMethods())
                    {
                        usedTypes.Add(method.ReturnType);
                        foreach (var param in method.GetParameters())
                            usedTypes.Add(param.ParameterType);
                    }
                    foreach (var property in type.GetProperties())
                        usedTypes.Add(property.PropertyType);
                    foreach (var evt in type.GetEvents())
                    {
                        usedTypes.Add(evt.EventHandlerType);
                        foreach (var gt in evt.EventHandlerType.GetGenericArguments())
                            usedTypes.Add(gt);
                    }
                }
            }

            // Assert
            foreach (var type in types)
                Assert.That(type.IsInterface || type.IsEnum || usedTypes.Contains(type));
        }
    }
}
