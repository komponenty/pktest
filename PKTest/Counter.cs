﻿using System;
using System.Collections.Generic;

namespace PK.Test
{
    public class Counter
    {
        private Dictionary<object, int> counts = new Dictionary<object, int>();

        public void Add(object o)
        {
            int c = 0;
            if (counts.ContainsKey(o))
                c = counts[o];
            counts.Add(o, c + 1);
        }

        public int Get(object o)
        {
            if (counts.ContainsKey(o))
                return counts[o];
            return 0;
        }
    }
}
